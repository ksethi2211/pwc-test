import React, { Component } from 'react';
import fire from './config/Fire';
import { Link } from 'react-router-dom';

class Login extends Component {

  constructor(props){
    super(props);
    this.login = this.login.bind(this);
    this.signup = this.signup.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.state={
      email:'',
      password:''
    }
  }

  login(e) {
    e.preventDefault();
    fire.auth().signInWithEmailAndPassword(this.state.email, this.state.password).then((u) =>{
    }).catch((error) => {
      console.log(error);
    })
  }

  signup(e) {
    e.preventDefault();
    fire.auth().createUserWithEmailAndPassword(this.state.email, this.state.password)
    .catch((error) => {
      console.log(error);
    })
  }

  handleChange(e) {
    this.setState({ [e.target.name]: e.target.value });
  }

  render() {
    return (
			<div className="row">
				<div className="col-6 bg">
				</div>
				<div className="col-5">
					<form>
					<hr />
					<h3 class="panel-title center">
              Welcome TO XD
            </h3>
						<hr />
						<br /><br />
						<div class="form-group">
							<label for="emailIn">
								Email Address:
							</label>
							<input 
								value={this.state.email} 
								onChange={this.handleChange} 
								type="email" 
								name="email" 
								class="form-control" 
								id="emailIn" 
								placeholder="Enter email"/>
							
								
						</div>
						<div class="form-group">
							<label for="passwordIn">
								Password:
							</label>
							<input 
								value={this.state.password} 
								onChange={this.handleChange} 
								type="password" 
								name="password" 
								class="form-control" 
								id="passwordIn"  
								placeholder="Enter password"/>
						</div>
						<button type="submit" onClick={this.login} class="btn btn-primary">Login</button>
						<button onClick={this.signup} style={{marginLeft:'25px'}} className="btn btn-success">SignUp</button><br /><br />
						<Link to={`/home`} class="btn btn-primary">&nbsp;&nbsp;Go to Admin view&nbsp;&nbsp;</Link>
					</form>
					
				</div>
			</div>
    )
  }
}

export default Login