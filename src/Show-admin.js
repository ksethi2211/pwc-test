import React, { Component } from 'react';
import fire from './config/Fire';
import { Link } from 'react-router-dom';

class ShowAdmin extends Component {

  constructor(props) {
    super(props);
    this.state = {
      complaint: {},
      key: ''
    };
  }

  componentDidMount() {
    const ref = fire.firestore().collection('complaints').doc(this.props.match.params.id);
    ref.get().then((doc) => {
      if (doc.exists) {
        this.setState({
          complaint: doc.data(),
          key: doc.id,
          isLoading: false
        });
      } else {
        console.log("No such document!");
      }
    });
  }

  render() {
    return (
      <div class="container">
        <hr />
        <div class="panel panel-default">
          <div class="panel-heading">

            <h3 class="panel-title">
              {this.state.complaint.issue}
            </h3>
          </div>
          <div class="panel-body">
            <dl>
              <dt>Description:</dt>
              <dd>{this.state.complaint.description}</dd>
							<dt>Resolve:</dt>
              <dd>{this.state.complaint.resolve}</dd>
              
            </dl>
            <Link to={`/edit-admin/${this.state.key}`} class="btn btn-success">Resolve Issue</Link><br /><br />
            <Link to={`/list-admin`} class="btn btn-success">Go back to Complaint List</Link>&nbsp;
          </div>
        </div>
      </div>
    );
  }
}

export default ShowAdmin;