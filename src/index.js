import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import App1 from './App1';
import Create from './Create';
import List from './List';
import ListAdmin from './List-admin';
import Show from './Show';
import ShowAdmin from './Show-admin';
import EditAdmin from './Edit-admin';

import * as serviceWorker from './serviceWorker';


import { BrowserRouter as Router, Route } from 'react-router-dom';


ReactDOM.render(
    <Router>
        <div>
          <Route path='/home' component={App} />
          <Route path='/user' component={App1} />
          <Route path='/create' component={Create} />
          <Route path='/list' component={List} />
          <Route path='/list-admin' component={ListAdmin} />
          <Route path='/show/:id' component={Show} />
          <Route path='/show-admin/:id' component={ShowAdmin} />
          <Route path='/edit-admin/:id' component={EditAdmin} />
          
        </div>
    </Router>,
    document.getElementById('root')
  );

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
