import React, { Component } from 'react';
import fire from './config/Fire';
import { Link } from 'react-router-dom';

class EditAdmin extends Component {

  constructor(props) {
    super(props);
    this.state = {
      complaint: {},
      key: '',
      issue: '',
      description: ''
    };
  }

  toggleChangeR = () => {
    this.setState(prevState => ({
      isResolved: !prevState.isResolved,
    }));
  }

  toggleChangeD = () => {
    this.setState(prevState => ({
      isDismissed: !prevState.isDismissed,
    }));
  }

  componentDidMount() {
    const ref = fire.firestore().collection('complaints').doc(this.props.match.params.id);
    ref.get().then((doc) => {
      if (doc.exists) {
        const complaint = doc.data();
        this.setState({
          complaint: doc.data(),
          key: doc.id,
          issue: complaint.issue,
          description: complaint.description,
          isCC: complaint.isCC,
          isP: complaint.isP,
          resolve: complaint.resolve,
          isDismissed: complaint.isDismissed,
          isResolved: complaint.isResolved
        });
      } else {
        console.log("No such document!");
      }
    });
  }

  onChange = (e) => {
    const state = this.state
    state[e.target.name] = e.target.value;
    this.setState({object:state});
  }

  onSubmit = (e) => {
    e.preventDefault();

    const { issue, description, isCC, isP, resolve, isDismissed, isResolved } = this.state;

    const updateRef = fire.firestore().collection('complaints').doc(this.state.key);
    updateRef.set({
      issue,
      description,
      resolve,
      isCC,
      isP,
      isDismissed,
      isResolved
    }).then((docRef) => {
      this.setState({
        issue,
        description,
        resolve: '',
        isDismissed: false,
        isResolved: false,
      });
      this.props.history.push("/show-admin/"+this.props.match.params.id)
    })
    .catch((error) => {
      console.error("Error adding document: ", error);
    });
  }

  render() {
    return (
      <div class="container">
        <hr />
        <div class="panel panel-default">
          <div class="panel-heading">
            <h3 class="panel-title">
              Resolve Complaint
            </h3>
          </div>
          <div class="panel-body">
            <h4><Link to={`/list-admin`} class="btn btn-primary">Complaints List</Link></h4>
            <form onSubmit={this.onSubmit}>
            <div class="panel-heading">
              <h3 class="panel-title">
                {this.state.complaint.issue}
              </h3>
              </div>
                <div class="panel-body">
                  <dl>
                    <dt>Description:</dt>
                    <dd>{this.state.complaint.description}</dd>
                  </dl>
                </div>
              <div class="form-group">
                <label for="resolve">Resolve:</label>
                <input type="text" class="form-control" name="resolve" value={this.state.resolve} onChange={this.onChange} placeholder="Enter a resolve" />
              </div>
              <div class="form-group">
							<label for="type">Labels for Complaints:</label><br />
								<div className="form-check">
										<label className="form-check-label">
											<input type="checkbox"
												checked={this.state.isResolved}
												onChange={this.toggleChangeR}
												className="form-check-input"
											/>
										Resolved
									</label>
								</div>
								<div className="form-check">
									<label className="form-check-label">
										<input type="checkbox"
											checked={this.state.isDismissed}
											onChange={this.toggleChangeD}
											className="form-check-input"
										/>
										Dismissed
									</label>
								</div>
              </div>
              <Link to={`/home`} class="btn btn-primary">Home</Link> &nbsp;&nbsp;
              <Link to={`/show-admin/${this.state.key}`} class="btn btn-primary">Back</Link> &nbsp;&nbsp;
              <button type="submit" class="btn btn-success">Submit</button>
            </form>
          </div>
        </div>
      </div>
    );
  }
}

export default EditAdmin;