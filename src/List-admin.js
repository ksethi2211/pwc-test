import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import fire from './config/Fire';

class ListAdmin extends Component {
  constructor(props) {
    super(props);
    this.ref = fire.firestore().collection('complaints');
    this.unsubscribe = null;
    this.state = {
      complaints: []
    };
  }

  handleRedirect(){
    this.props.history.push('/home');
  }

  onCollectionUpdate = (querySnapshot) => {
    const complaints = [];
    querySnapshot.forEach((doc) => {
      const { issue, description, isCC, isP } = doc.data();
      complaints.push({
        key: doc.id,
        issue, // DocumentSnapshot
				description,
				isCC,
				isP
      });
    });
    this.setState({
      complaints
   });
  }

  componentDidMount() {
    this.unsubscribe = this.ref.onSnapshot(this.onCollectionUpdate);
  }

  render() {
    return (
      <div class="container">
			<hr />
        <div class="panel panel-default">
          <div class="panel-heading">
            <h3 class="panel-title">
              COMPLAINTS LIST
            </h3>
          </div>
          <div class="panel-body">
            <table class="table table-stripe">
              <thead>
                <tr>
                  <th width="80%">Issue</th>
									<th><font color="white">Resolved</font></th>

                </tr>
              </thead>
              <tbody>
                {this.state.complaints.map(complaint =>
                  <tr>
                    <td><Link to={`/show-admin/${complaint.key}`}>{complaint.issue}</Link></td>
										<td><span class="badge badge-primary">{complaint.isCC && String("Customer Care")}</span>
										<span class="badge badge-primary">{complaint.isP && String("Product Related")}</span></td>
                  </tr>
                )}
              </tbody>
            </table>
            <Link to={`/home`} class="btn btn-primary">Home</Link> &nbsp;&nbsp;
          </div>
        </div>
      </div>
    );
  }
}

export default ListAdmin;