import React, { Component } from 'react';
import { Link } from 'react-router-dom';

export class App extends Component {
	render() {
		return (
			<div className="centered">
				<Link to={`/list-admin`} class="btn btn-success">Admin</Link>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				<Link to={`/user`} class="btn btn-success">&nbsp;User&nbsp;</Link>
			</div>
		)
	}
}

export default App
