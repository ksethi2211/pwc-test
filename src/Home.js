import React, { Component } from 'react'
import { Link } from 'react-router-dom';
import './Styles.css';
import fire from './config/Fire';

export class Home extends Component {
  logout(){
    fire.auth().signOut();
  }
  render() {

    return (
      <div className="col-md-6">
			<hr />
        <button onClick={this.logout} className="btn btn-primary FixedButton">Logout</button>
          <h4><Link to="/create">Create New Compaint</Link></h4><br />
          <h4><Link to="/list">View Existing Complaints</Link></h4>

      </div>
        )
    }
}

export default Home