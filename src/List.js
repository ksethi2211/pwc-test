import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import fire from './config/Fire';

class List extends Component {
  constructor(props) {
    super(props);
    this.ref = fire.firestore().collection('complaints');
    this.unsubscribe = null;
    this.state = {
      complaints: []
    };
  }

  handleRedirect(){
    this.props.history.push('/home');
  }

  onCollectionUpdate = (querySnapshot) => {
    const complaints = [];
    querySnapshot.forEach((doc) => {
      const { issue, description, isResolved, isDismissed } = doc.data();
      complaints.push({
        key: doc.id,
        issue, // DocumentSnapshot
				description,
				isResolved,
				isDismissed
      });
    });
    this.setState({
      complaints
   });
  }

  componentDidMount() {
    this.unsubscribe = this.ref.onSnapshot(this.onCollectionUpdate);
  }

  render() {
    return (
      <div class="container">
			<hr />
        <div class="panel panel-default">
          <div class="panel-heading">
            <h3 class="panel-title">
              COMPLAINTS LIST
            </h3>
          </div>
          <div class="panel-body">
            <h4><Link to="/create">Add Complaint</Link></h4>
            <table class="table table-stripe">
              <thead>
                <tr>
                  <th width="90%">Issue</th>
									<th><font color="white">Resolved</font></th>

                </tr>
              </thead>
              <tbody>
                {this.state.complaints.map(complaint =>
                  <tr>
                    <td><Link to={`/show/${complaint.key}`}>{complaint.issue}</Link></td>
										<td><span class="badge badge-primary">{complaint.isResolved && String("Resolved")}</span>
										<span class="badge badge-danger">{complaint.isDismissed && String("Dismissed")}</span></td>
                  </tr>
                )}
              </tbody>
            </table>
            <Link to={`/user`} class="btn btn-primary">Home</Link> &nbsp;&nbsp;
          </div>
        </div>
      </div>
    );
  }
}

export default List;