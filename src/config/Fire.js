import firebase from 'firebase';

const config = {
  apiKey: "AIzaSyBcu-VOaz5zB8mxOlfPwiN94G0ua6KGbto",
  authDomain: "proj1-77917.firebaseapp.com",
  databaseURL: "https://proj1-77917.firebaseio.com",
  projectId: "proj1-77917",
  storageBucket: "proj1-77917.appspot.com",
  messagingSenderId: "1036600818831",
  appId: "1:1036600818831:web:95befe38ac81c0b3"
};

const fire = firebase.initializeApp(config);

export default fire;