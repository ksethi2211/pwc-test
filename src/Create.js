import React, { Component } from 'react';
import fire from './config/Fire';
import { Link } from 'react-router-dom';

class Create extends Component {

	constructor() {
    super();
    this.ref = fire.firestore().collection('complaints');
    this.state = {
      issue: '',
			description: '',
			isCC: false,
			isP: false,
			isResolved: false,
			isDismissed: false
    };
	}

	toggleChangeCC = () => {
    this.setState(prevState => ({
      isCC: !prevState.isCC,
    }));
  }

  toggleChangeP = () => {
    this.setState(prevState => ({
      isP: !prevState.isP,
    }));
  }
	
  onChange = (e) => {
    const state = this.state
    state[e.target.name] = e.target.value;
    this.setState(state);
  }

	onSubmit = (e) => {
    e.preventDefault();

    const { issue, description, isCC, isP, isResolved, isDismissed } = this.state;

    this.ref.add({
      issue,
			description,
			isCC,
			isP,
			isResolved,
			isDismissed
    }).then((docRef) => {
      this.setState({
        issue: '',
				description: '',
				isCC: false,
				isP: false
      });
      this.props.history.push("/user")
		})
		
    .catch((error) => {
      console.error("Error adding document: ", error);
    });
	}
	
	
	render() {
		const { issue, description, isCC, isP } = this.state;
    return (
      <div class="container">
        <hr />
        <div class="panel panel-default">
          <div class="panel-heading">
            <h3 class="panel-title">
              Enter New Object
              
            </h3>
          </div>
          <div class="panel-body">
            
            <form onSubmit={this.onSubmit}>
              <div class="form-group">
                <label for="issue">Issue:</label>
                <input type="text" class="form-control" name="issue" value={issue} onChange={this.onChange} placeholder="Describe your issue in 1-2 lines." />
              </div>
              <div class="form-group">
							<label for="type">Labels for Complaints:</label><br />
								<div className="form-check">
										<label className="form-check-label">
											<input type="checkbox"
												checked={this.state.isCC}
												onChange={this.toggleChangeCC}
												className="form-check-input"
											/>
										Customer Care
									</label>
								</div>
								<div className="form-check">
									<label className="form-check-label">
										<input type="checkbox"
											checked={this.state.isP}
											onChange={this.toggleChangeP}
											className="form-check-input"
										/>
										Product Related
									</label>
								</div>
              </div>
              <div class="form-group">
                <label for="description">Description:</label>
                <textArea class="form-control" name="description" onChange={this.onChange} placeholder="Description" cols="80" rows="3">{description}</textArea>
              </div>
							<Link to={`/user`} class="btn btn-primary">Home</Link> &nbsp;&nbsp;
              <button type="submit" class="btn btn-success">Submit</button>
            </form>
          </div>
        </div>
      </div>
    );
  }
}

export default Create
